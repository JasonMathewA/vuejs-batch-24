//soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//saya senang belajar JAVASCRIPT

var satu = pertama.substr(0,4);
var dua = pertama.substr(12,6);

var ketiga = kedua.substr(0,7);
var keempat = kedua.substr(8,10);

var keempat_caps = keempat.toUpperCase();

var combined = satu + " " + dua + " " + ketiga + " " + keempat_caps

console.log(combined) // .....jawaban soal 1

//soal 2

var kataPertama =  Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("6");

var a = kataPertama + kataKeempat;
var b = kataKedua * kataKetiga;

var result = a + b;

console.log(result)// ...... jawaban soal 2

//soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0,3);
var kataKedua = kalimat.substring(4,14); 
var kataKetiga = kalimat.substring(15,18); 
var kataKeempat = kalimat.substring(19,25); 
var kataKelima = kalimat.substring(25,31); 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima); // .....jawaban soal 3
